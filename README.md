This application is a scala based application.

In order to run it, you must install sbt:

`$ brew install sbt`

## Why Scala?

Scala provides an out of the box parallelism implemented on all the iterators used.

Because most of the operations to handle big data require data transformation (`map`), simply using `par` in the sequence we will execute all transformations in parallel. 
It will increase application performance.

Another extra value provided by scala is the variable immutability, which is critical for data processing and HA applications.   

## Running the app!

Inside the application root folder, execute:

`$ sbt run`

This process will pull all the dependencies and it will run the application.

Once the application is running, it will prompt for a port number. The current range is between 3000 and 9999.
Please try to don't use an used port.

## Running Tests
There are +30 tests running covering the relevant classes/methods used to build the application

`$ sbt test`

## Recommendations

This application was develop using JAVA 8. If you are using a different version of the JVM and you notice weird behaviour, please switch the java version.