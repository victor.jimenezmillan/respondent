package helpers

import com.typesafe.config.ConfigFactory
import models.WeightMethod
import processors.AffinityProcessor
import services.csv.RespondentDataLoader
import services.json.OfferDataLoader
import types.AffinityProcessorTypes

trait MainStartConfig {
  val config = ConfigFactory.load()
  implicit val maxDistance: Int = config.getInt("candidate.distance")
  val CSV_FILE_NAME = config.getString("file.candidates")
  val OFFER_FILE = config.getString("file.offer")
  val offer = new OfferDataLoader(OFFER_FILE).content.get
  val respondentDataLoader = new RespondentDataLoader(CSV_FILE_NAME, config.getString("file.csv_separator"))
  val affinityProcessor = new AffinityProcessor(respondentDataLoader.content, offer , config.getInt("candidate.max_score_per_block"))
  val criteria = Array(
    WeightMethod(AffinityProcessorTypes.BasedOnDistance, config.getBoolean("criteria.BasedOnDistance")),
    WeightMethod(AffinityProcessorTypes.BaseOnJobTitle, config.getBoolean("criteria.BaseOnJobTitle")),
    WeightMethod(AffinityProcessorTypes.BasedOnIndustry, config.getBoolean("criteria.BasedOnIndustry"))

  )
}
