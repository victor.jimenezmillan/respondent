package helpers

trait ContentLoader[T] {
  val content: T
}
