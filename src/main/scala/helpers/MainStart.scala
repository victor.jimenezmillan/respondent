package helpers

import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.io.StdIn

object MainStart extends LazyLogging{

  @tailrec
  def introduceValue(): Int = {
    println("Please introduce port to run application [3000-9999]")
    try {
      val input = StdIn.readInt()
      if (input >= 3000 && input <= 9999) input else throw new Exception("Port out of range")
    } catch {
      case ex: Exception =>
        logger.error(s"Invalid port number: ${ex.getMessage}")
        introduceValue()
    }
  }
}
