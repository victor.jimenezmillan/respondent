package models

case class DistanceScore(distance: Double, score: Int)
