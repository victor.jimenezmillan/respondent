package models

import play.api.libs.json.Json

case class Location(location: LocationDescription)

object Location {
  implicit val fmt = Json.format[Location]
}
