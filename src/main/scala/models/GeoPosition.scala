package models

import play.api.libs.json.Json

case class GeoPosition(latitude: Double, longitude: Double)

object GeoPosition {
  implicit val fmt = Json.format[GeoPosition]
}
