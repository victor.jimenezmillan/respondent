package models

import play.api.libs.json.Json

case class RespondentDataProcessed(name: String, distance: Double, score: Int)

object RespondentDataProcessed {
  implicit val fmt = Json.format[RespondentDataProcessed]
}
