package models

import play.api.libs.json.Json

case class LocationDescription(
                                id: String,
                                city: String,
                                state: String,
                                country: String,
                                formattedAddress: String,
                                location: GeoPosition
                              )

object LocationDescription {
  implicit val fmt = Json.format[LocationDescription]
}
