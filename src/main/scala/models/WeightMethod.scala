package models

case class WeightMethod(affinityCalculatorMethod: String, excludeCandidate: Boolean = false)
