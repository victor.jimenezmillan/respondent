package models

case class RespondentData(
                           firstName: String,
                           gender: String,
                           jobTitle: String,
                           industry: String,
                           city: String,
                           latitude: Double,
                           longitude: Double
                         )
