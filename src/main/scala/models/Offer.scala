package models

import play.api.libs.json.Json

case class Offer(
                  numberOfParticipants: Int,
                  timezone: String,
                  genders: String,
                  country: String,
                  incentive: Int,
                  name: String,
                  professionalJobTitles: Array[String],
                  professionalIndustry: Array[String],
                  education: Array[String],
                  cities: Array[Location]
                )

object Offer {
  implicit val fmt = Json.format[Offer]
}
