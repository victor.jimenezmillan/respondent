package services.file

import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.reflect.ClassTag

object FileReader extends FileReaderContract with LazyLogging {
  def readFileFromSource(fileName: String): Option[Iterator[String]] = {
    val file = Source.fromResource(fileName)
    logger.info(s"Readed file from sources: $fileName")
    try {
      if(file.isEmpty) None else Some(file.getLines())
    } catch {
      case exception: Exception =>
        logger.error(s"Impossible to load file $fileName: ${exception.getMessage}")
        None
    }
  }

  def processFileToSeqOfObjects[T: ClassTag](callback: ArrayBuffer[String] => Array[T], resourceName: String): Array[T] = {
    val resourceFile = readFileFromSource(resourceName)
    logger.info(s"Processing file from sources: $resourceName")
    if(resourceFile.isEmpty) {
      Array.empty[T]
    } else {
      try {
        val tempResources: ArrayBuffer[String] = resourceFile.get.to[ArrayBuffer]
        callback(tempResources)
      } catch {
        case ex: Exception =>
          logger.error(s"Impossible to parse file to RespondentDataModel: ${ex.getMessage}")
          ArrayBuffer[T]().toArray
      }
    }
  }
}
