package services.file

trait FileReaderContract {
  def readFileFromSource(fileName: String): Option[Iterator[String]]
}
