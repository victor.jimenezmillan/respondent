package services.csv

import helpers.ContentLoader
import models.RespondentData

import scala.collection.mutable.ArrayBuffer
import services.file.FileReader

class RespondentDataLoader(resourceName: String, separator: String) extends ContentLoader[Array[RespondentData]]{
  private def extractRespondentData(tempResources: ArrayBuffer[String]): Array[RespondentData] = {
    if(tempResources.head.contains("firstName")) tempResources.remove(0)
    tempResources.toArray.map { line =>
      val fields = line.split(s"$separator" +"(?=([^\"]*\"[^\"]*\")*[^\"]*$)").map(_.trim)
      RespondentData(fields(0), fields(1), fields(2), fields(3), fields(4), fields(5).toDouble, fields(6).toDouble)
    }
  }

  override val content: Array[RespondentData] = FileReader.processFileToSeqOfObjects[RespondentData](extractRespondentData, resourceName)
}
