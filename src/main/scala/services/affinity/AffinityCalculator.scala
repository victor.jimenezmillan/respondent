package services.affinity

import com.typesafe.scalalogging.LazyLogging
import models.{DistanceScore, GeoPosition, Offer, RespondentData}
import services.distance.DistanceCalculator

abstract class AffinityCalculator(offer: Offer, functionMeasure: Int)(implicit maxDistance: Int) extends LazyLogging{
  private val MAX_NUMBER_COINCIDENCES_INDUSTRY = offer.professionalIndustry.length

  private def distanceMeasureCalculator(distance: Double): Int = {
    if(distance >= maxDistance) {
      0
    } else {
      Math.round(((maxDistance - distance) * functionMeasure) / maxDistance).toInt
    }
  }

  def basedOnIndustry(respondentData: RespondentData): Int = {
    logger.debug(s"Calculating Based on industry from ${respondentData.industry}")
    val skills = respondentData.industry.toLowerCase.split(",").map(_.trim.replaceAll("\"",""))
    val notMatches = offer.professionalIndustry.map(_.toLowerCase).filterNot(skills.toSet)
    val coincidencesNumber = MAX_NUMBER_COINCIDENCES_INDUSTRY - notMatches.length
    Math.round((coincidencesNumber * functionMeasure) / MAX_NUMBER_COINCIDENCES_INDUSTRY)
  }

  def baseOnJobTitle(respondentData: RespondentData): Int = {
    logger.debug(s"calculating Job Title from ${respondentData.jobTitle}")
    if(offer.professionalJobTitles.par.map(_.toLowerCase).exists(title => title.contains(respondentData.jobTitle.toLowerCase) || respondentData.jobTitle.toLowerCase.contains(title))) {
      functionMeasure
    } else {
      0
    }
  }

  def basedOnDistance(respondentData: RespondentData): DistanceScore = {
    logger.debug(s"Calculating distance from LAT: ${respondentData.latitude}, LON: ${respondentData.longitude}")
    val geoApplicant = GeoPosition(respondentData.latitude, respondentData.longitude)
    val distance = offer.cities.par.map { city =>
      val geoCity = GeoPosition(city.location.location.latitude, city.location.location.longitude)
      DistanceCalculator.from2GeoPositions(geoApplicant, geoCity)
    }.min

    val score = if(distance == 0) {
      functionMeasure
    } else {
      distanceMeasureCalculator(distance)
    }

    DistanceScore(distance, score)
  }
}
