package services.distance

import models.GeoPosition

object DistanceCalculator extends DistanceCalculatorContract {
  private val HEART_RAD = 6371
  private val CONVERSION_FACTOR = 1000

  def from2GeoPositions(geo1: GeoPosition, geo2: GeoPosition): Double = {
    val latFirstRads = geo1.latitude.toRadians
    val latSecondRads = geo2.latitude.toRadians
    val deltaLongitude = (geo2.longitude - geo1.longitude).toRadians

    val arc = Math.acos(Math.sin(latFirstRads) * Math.sin(latSecondRads) + Math.cos(latFirstRads) * Math.cos(latSecondRads) * Math.cos(deltaLongitude))

    (math rint (HEART_RAD * arc) * CONVERSION_FACTOR) / CONVERSION_FACTOR
  }
}
