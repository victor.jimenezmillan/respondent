package services.distance

import models.GeoPosition

trait DistanceCalculatorContract {
  def from2GeoPositions(geo1: GeoPosition, geo2: GeoPosition): Double
}
