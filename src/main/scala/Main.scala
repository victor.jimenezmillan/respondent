import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json
import helpers._


object Main extends App with  LazyLogging with MainStartConfig{
  implicit val actorSystem = ActorSystem()
  implicit val executionContext = actorSystem.dispatcher
  implicit val mt = ActorMaterializer()

  val port = MainStart.introduceValue()
  val route = path("candidates"){
    complete(Json.toJson(affinityProcessor.processBasedOnCriteria(criteria)).toString())
  }

  Http().bindAndHandle(route, "0.0.0.0", port)

  logger.info(s"Started the application in http://localhost:$port/candidates using sources/$CSV_FILE_NAME and sources/$OFFER_FILE ")
  logger.info(s"To use other data-source, please override the files: Future improvement -> Load sources on runtime")
}
