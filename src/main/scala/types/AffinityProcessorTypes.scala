package types

object AffinityProcessorTypes {
  val BasedOnIndustry: String = "basedOnIndustry"
  val BaseOnJobTitle: String = "baseOnJobTitle"
  val BasedOnDistance: String = "basedOnDistance"
}
