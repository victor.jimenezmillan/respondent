package processors

import models._
import services.affinity.AffinityCalculator
import types.AffinityProcessorTypes

class AffinityProcessor(candidates: Array[RespondentData], offer: Offer, measure: Int)(implicit maxDistance: Int) extends AffinityCalculator(offer, measure){
  sealed case class IntermediateResults(value: Int, exclude: Boolean)

  private def methodExecution(candidate: RespondentData, method: WeightMethod): Int = {
    method.affinityCalculatorMethod match {
      case AffinityProcessorTypes.BasedOnIndustry => basedOnIndustry(candidate)
      case AffinityProcessorTypes.BaseOnJobTitle => baseOnJobTitle(candidate)
      case AffinityProcessorTypes.BasedOnDistance => basedOnDistance(candidate).score
    }
  }

  private def calculateIntermediateResult(intermediateResults: IntermediateResults): Int = {
    intermediateResults.value match {
      case 0 => if (intermediateResults.exclude) -1 else 0
      case a: Int => a
    }
  }

  def processCandidate(candidate: RespondentData, methodsToProcess: Array[WeightMethod]): RespondentDataProcessed = {
    val intermediateResults: Seq[Int] = methodsToProcess.map(value1 => calculateIntermediateResult(IntermediateResults(methodExecution(candidate, value1), value1.excludeCandidate)))
    RespondentDataProcessed(candidate.firstName, basedOnDistance(candidate).distance, if(intermediateResults.contains(-1)) 0 else intermediateResults.sum)
  }

  def processBasedOnCriteria(methodsToProcess: Array[WeightMethod]): Array[RespondentDataProcessed] = {
    candidates.par.map(processCandidate(_, methodsToProcess)).filterNot(_.score == 0).toArray.sortWith(_.score > _.score)
  }
}
