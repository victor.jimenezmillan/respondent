package processors

import models.{RespondentData, WeightMethod}
import org.scalatest.{FlatSpec, Matchers}
import services.csv.RespondentDataLoader
import services.json.OfferDataLoader
import types.AffinityProcessorTypes

class AffinityProcessorSpec extends FlatSpec with Matchers {
  implicit val maxDistance: Int = 100
  private val CSV_FILE_NAME = "respondents_data_test.csv"
  private val offer = new OfferDataLoader("project.json").content.get
  private val respondentDataLoader = new RespondentDataLoader(CSV_FILE_NAME, ",")
  private val affinityProcessor = new AffinityProcessor(respondentDataLoader.content, offer , 33)

  val criteria = Array(
    WeightMethod(AffinityProcessorTypes.BasedOnDistance, true),
    WeightMethod(AffinityProcessorTypes.BaseOnJobTitle, true),
    WeightMethod(AffinityProcessorTypes.BasedOnIndustry, false)
  )

  "processCandidate" should "process the first candidate properly" in {
    // Candidate in NYC
    val result = affinityProcessor.processCandidate(respondentDataLoader.content.head, criteria)
    result.score > 0 shouldBe true
    result.distance shouldBe 0
  }

  it should "exclude score with 0 to a candidate in Spain" in {
    val developerFromMadrid = RespondentData("foo", "foo", "Developer", "Banking", "Madrid", 40.416775, -3.703790)
    affinityProcessor.processCandidate(developerFromMadrid, criteria).score shouldBe 0
  }

  it should "score properly a developer based on NYC but with fake industry" in {
    val developer = RespondentData("foo", "foo", "Developer", "foo", "NYC", 40.730610, 	-73.935242)
    val candidateProcessed = affinityProcessor.processCandidate(developer, criteria)
    candidateProcessed.distance > 0 shouldBe true
    candidateProcessed.score > 0 shouldBe true
  }

  it should "score properly a developer based on Washington but with fake industry" in {
    val developer = RespondentData("foo", "foo", "Developer", "foo", "Washington", 38.9071923, 	-78.0368707)
    val candidateProcessed = affinityProcessor.processCandidate(developer, criteria)
    candidateProcessed.distance > 0 shouldBe true
    candidateProcessed.score > 0 shouldBe true
  }

  "processBasedOnCriteria" should "return an array with less candidates that the original list of 500 people" in {
    val candidates = affinityProcessor.processBasedOnCriteria(criteria)
    candidates.length > 0 shouldBe true
    candidates.length < 500 shouldBe true
  }

  it should "sorted descending scores" in {
    val candidates = affinityProcessor.processBasedOnCriteria(criteria)
    candidates.head.score > candidates.last.score shouldBe true
  }
}
