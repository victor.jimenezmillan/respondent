package services.distance

import models.GeoPosition
import org.scalatest.{FlatSpec, Matchers}

class DistanceCalculatorSpec extends FlatSpec with Matchers {
  private val distanceCalculator = DistanceCalculator

  "from2GeoPositions" should "provide a valid distance between 2 known positions" in {
    distanceCalculator.from2GeoPositions(GeoPosition(40.7127753, -74.0059728), GeoPosition(40.6781784, -73.9441579)) shouldBe 6.478
  }

  it should "be 0 if the 2 points are the same" in {
    distanceCalculator.from2GeoPositions(GeoPosition(40.7127753, -74.0059728), GeoPosition(40.7127753, -74.0059728)) shouldBe 0
  }

  it should "should be half perimeter if position is from pole to pole (assuming hearth is a sphere)" in  {
    Math.round(distanceCalculator.from2GeoPositions(GeoPosition(90,0), GeoPosition(-90,0))) shouldBe Math.round(6371*Math.PI)
  }

}
