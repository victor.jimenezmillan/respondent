package services.file

import models.RespondentData
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable.ArrayBuffer

class FileReaderSpec extends FlatSpec with Matchers{
  private val CSV_FILE_NAME = "respondents_data_test.csv"

  private val fileReader = FileReader

  "ReadFileFromSource" should "read the content of qualified file and return a list of strings" in {
    val csv = fileReader.readFileFromSource(CSV_FILE_NAME)
    csv.isEmpty shouldBe false
    csv.get.isEmpty shouldBe false
  }

  it should "return None if file doesn't exist" in {
    val falseCsv = fileReader.readFileFromSource("foo")
    falseCsv.isEmpty shouldBe true
  }

  def simpleArray(test: ArrayBuffer[String]): Array[String] = test.toArray

  "ProcessFileToSeqOfObjects" should "generate an array of string based on a simple callback from a valid source" in {
    fileReader.processFileToSeqOfObjects[String](simpleArray, CSV_FILE_NAME).length shouldBe 501
  }

  it should "generate an empty array if source is not valid" in {
    fileReader.processFileToSeqOfObjects[String](simpleArray, "foo").length shouldBe 0
  }

  it should "generate an array of objects using a complex callback from a valid source" in {
    def complexArray(test: ArrayBuffer[String]): Array[RespondentData] = {
      if(test.head.contains("firstName")) test.remove(0)
      test.toArray.map { line =>
        val fields = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)").map(_.trim)
        RespondentData(fields(0), fields(1), fields(2), fields(3), fields(4), fields(5).toDouble, fields(6).toDouble)
      }
    }

    fileReader.processFileToSeqOfObjects[RespondentData](complexArray, CSV_FILE_NAME).length shouldBe 500
  }
}
