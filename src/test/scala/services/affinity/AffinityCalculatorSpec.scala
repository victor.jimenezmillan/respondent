package services.affinity

import models.RespondentData
import org.scalatest.{FlatSpec, Matchers}
import services.json.OfferDataLoader

class AffinityCalculatorSpec extends FlatSpec with Matchers {
  implicit val maxDistance: Int = 100
  sealed class FakeProcessor(measure: Int) extends AffinityCalculator(new OfferDataLoader("project.json").content.get, measure)

  private val affinityCalculator = new FakeProcessor(100)
  private val affinityCalculator33 = new FakeProcessor(33)
  private val MAX_DISTANCE = 100

  "basedOnIndustry with 100" should "returns 0 for no matches" in {
    affinityCalculator.basedOnIndustry(RespondentData("foo", "foo", "foo", "foo", "foo", 0, 0)) shouldBe 0
  }

  it should "returns 100 for all the coincidences" in {
    val full = "Banking, Financial Services, Government Administration, Insurance, Retail, Supermarkets, Automotive, Computer Software"
    affinityCalculator.basedOnIndustry(RespondentData("foo", "foo", "foo", full, "foo", 0, 0)) shouldBe 100
  }

  it should "returns 50 for half of coincidences" in {
    val half = "Retail, Supermarkets, Automotive, Computer Software"
    affinityCalculator.basedOnIndustry(RespondentData("foo", "foo", "foo", half, "foo", 0, 0)) shouldBe 50
  }

  it should "returns 37 for 3 coincidences" in {
    val test = "Supermarkets, Automotive, Computer Software"
    affinityCalculator.basedOnIndustry(RespondentData("foo", "foo", "foo", test, "foo", 0, 0)) shouldBe 37
  }

  it should "returns 62 for 3 coincidences" in {
    val test = "Banking, Retail,Supermarkets, Automotive, Computer Software"
    affinityCalculator.basedOnIndustry(RespondentData("foo", "foo", "foo", test, "foo", 0, 0)) shouldBe 62
  }

  "basedOnIndustry with 33" should "returns 0 for no matches" in {
    affinityCalculator33.basedOnIndustry(RespondentData("foo", "foo", "foo", "foo", "foo", 0, 0)) shouldBe 0
  }

  it should "returns 33 for all the coincidences" in {
    val full = "Banking, Financial Services, Government Administration, Insurance, Retail, Supermarkets, Automotive, Computer Software"
    affinityCalculator33.basedOnIndustry(RespondentData("foo", "foo", "foo", full, "foo", 0, 0)) shouldBe 33
  }

  it should "returns 16 for half of coincidences" in {
    val half = "Retail, Supermarkets, Automotive, Computer Software"
    affinityCalculator33.basedOnIndustry(RespondentData("foo", "foo", "foo", half, "foo", 0, 0)) shouldBe 16
  }

  it should "returns 12 for 3 coincidences" in {
    val test = "Supermarkets, Automotive, Computer Software"
    affinityCalculator33.basedOnIndustry(RespondentData("foo", "foo", "foo", test, "foo", 0, 0)) shouldBe 12
  }

  it should "returns 62 for 5 coincidences" in {
    val test = "Banking, Retail,Supermarkets, Automotive, Computer Software"
    affinityCalculator33.basedOnIndustry(RespondentData("foo", "foo", "foo", test, "foo", 0, 0)) shouldBe 20
  }

  "baseOnJobTitle with 33" should "returns 0 once the job title doesn't match" in {
    affinityCalculator33.baseOnJobTitle(RespondentData("foo", "foo", "foo", "0", "foo", 0, 0)) shouldBe 0
  }

  it should "returns 33 for a job match" in  {
    affinityCalculator33.baseOnJobTitle(RespondentData("foo", "foo", "Programmer", "0", "foo", 0, 0)) shouldBe 33
    affinityCalculator33.baseOnJobTitle(RespondentData("foo", "foo", "Sr Software Engineer", "0", "foo", 0, 0)) shouldBe 33
  }

  "basedOnDistance" should "returns 33 if distance is 0" in  {
    val distanceCalc = affinityCalculator33.basedOnDistance(RespondentData("foo", "foo", "Programmer", "0", "foo", 38.9071923, -77.0368707))
    distanceCalc.score shouldBe 33
    distanceCalc.distance.toInt shouldBe 0
  }
}
