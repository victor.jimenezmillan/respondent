package services.csv

import org.scalatest.{FlatSpec, Matchers}

class RespondentDataLoaderSpec extends FlatSpec with Matchers {
  private val CSV_FILE_NAME = "respondents_data_test.csv"

  "RespondentDataLoader" should "load data properly from the valid resource file" in  {
    val respondentDataLoader = new RespondentDataLoader(CSV_FILE_NAME, ",")
    respondentDataLoader.content.isEmpty shouldBe false
    respondentDataLoader.content.length shouldBe 500
  }

  it should "return an empty array if the file content is not valid/file doesn't exist" in {
    val respondentDataLoader = new RespondentDataLoader("foo",",")
    respondentDataLoader.content.isEmpty shouldBe true
  }

}
