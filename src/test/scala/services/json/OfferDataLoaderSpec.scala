package services.json

import org.scalatest.{FlatSpec, Matchers}

class OfferDataLoaderSpec extends FlatSpec with Matchers {
  "OfferDataLoader" should "Load properly a resource file and parse in array of string in content" in {
    val offerDataLoader = new OfferDataLoader("project.json")
    offerDataLoader.content.get.genders shouldBe "N/A"
    offerDataLoader.content.get.cities.length shouldBe 8
    offerDataLoader.content.get.cities.head.location.location.latitude > 0 shouldBe true
  }

  it should "not load anything if the resource file is invalid" in  {
    val offerDataLoaderFake = new OfferDataLoader("foo.json")
    offerDataLoaderFake.content shouldBe None
  }
}
